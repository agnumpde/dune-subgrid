#ifndef DUNE_SUBGRID_ENTITY_SEED_HH
#define DUNE_SUBGRID_ENTITY_SEED_HH

/**
 * \file
 * \brief The SubGridEntitySeed class
 */


namespace Dune {


/**
 * \brief The EntitySeed class provides the minimal information needed to restore an Entity using the grid.
 * \ingroup SubGrid
 *
 */
template<int codim, class GridImp>
class SubGridEntitySeed
{
    protected:

        // Entity type of the hostgrid
        typedef typename GridImp::HostGridType::Traits::template Codim<codim>::Entity HostEntity;

        // EntitySeed type of the hostgrid
        typedef typename GridImp::HostGridType::Traits::template Codim<codim>::EntitySeed HostEntitySeed;

    public:

        enum {codimension = codim};

        /**
         * \brief Create EntitySeed from hostgrid Entity
         *
         * We call hostEntity.seed() directly in the constructor
         * of SubGridEntitySeed to allow for return value optimization.
         *
         * If we used SubGridEntitySeed(hostEntity.seed())
         * we would have one copy even with optimization enabled.
         */
        SubGridEntitySeed(const HostEntity& hostEntity) :
            hostEntitySeed_(hostEntity.seed())
        {}

        /**
         * \brief Create invalid EntitySeed
         *
         * This calls the default constructor of the encapsulated
         * HostEntitySeed.
         */
        SubGridEntitySeed() :
            hostEntitySeed_()
        {}

        /**
         * \brief Get stored HostEntitySeed
         */
        const HostEntitySeed& hostEntitySeed() const
        {
            return hostEntitySeed_;
        }

        /**
         * \brief Check if this EntitySeed is valid
         */
        bool isValid() const
        {
            return hostEntitySeed_.isValid();
        }

    private:

        HostEntitySeed hostEntitySeed_;
};

} // namespace Dune


#endif
